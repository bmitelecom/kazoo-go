# kazoo-go

Golang SDK implementation which works with Kazoo API by 2600hz. This project is aimed to provide a convenient and robust way to work with Kazoo API from Golang programs.